<?php

/**
 * @package     	Joomla.Site
 * @subpackage  	mod_menu override
 * @copyright   	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     	GNU General Public License version 2 or later; see LICENSE.txt
 * Modifications	Joomla CSS
 */

defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.
$class = $item->anchor_css ? 'class="'.$item->anchor_css.'" ' : '';
$title = $item->anchor_title ? 'title="'.$item->anchor_title.'" ' : '';

//CG Override: menu image muss im Fall active den img-file namen ändern zB image.png in image-active.png
$menIMG = $item->menu_image;
$activeState = "-active";
$activeIMG = preg_replace('/(\.[^.]+)$/', sprintf('%s$1', $activeState), $menIMG);

if ($item->menu_image)
{
	$item->params->get('menu_text', 1) ?

		in_array($item->id, $path) ? // ist parent li active? dann:

			$linktype = '<img src="'.$activeIMG.'" alt="'.$item->title.'" /><div class="image-title">'.$item->title.'</div> ' : 
		// else:
			$linktype = '<img src="'.$item->menu_image.'" alt="'.$item->title.'" /><div class="image-title">'.$item->title.'</div> ' :
		//Rest wie gehabt
			$linktype = '<img src="'.$item->menu_image.'" alt="'.$item->title.'" />';

			if ($item->deeper) {
			$class = 'class="'.$item->anchor_css.' dropdown-toggle" data-toggle="dropdown" ';
			//$item->flink = '#';
	}
}

	elseif ($item->deeper) { 
		$linktype = $item->title. '<b class="caret"></b>' ;
		if ($item->level < 2) {
		$class = 'class="'.$item->anchor_css.' dropdown-toggle" data-toggle="dropdown" ';
		//$item->flink = '#';
	}

	else {
		$linktype = $item->title;
	}
}

else {
	$linktype = $item->title;
}

switch ($item->browserNav) :
	default:
	case 0:
?><a <?php echo $class; ?>href="<?php echo $item->flink; ?>" <?php echo $title; ?>><span><?php echo $linktype; ?></span></a><?php
		break;

	case 1:
		// _blank
?><a <?php echo $class; ?>href="<?php echo $item->flink; ?>" target="_blank" <?php echo $title; ?>><span><?php echo $linktype; ?></span></a><?php
		break;

	case 2:
	// window.open

?><a <?php echo $class; ?>href="<?php echo $item->flink; ?>" onclick="window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes');return false;" <?php echo $title; ?>><span><?php echo $linktype; ?></span></a>
<?php
		break;

endswitch;

