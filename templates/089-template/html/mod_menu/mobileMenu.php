<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$id = '';

if (($tagId = $params->get('tag_id', '')))
{
	$id = ' id="' . $tagId . '"';
}

// The menu class is deprecated. Use nav instead
?>
<div id="myNav" class="overlay">
	<a id="mobMenClose" href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
		<div class="overlay-content">
			<ul class="menu<?php echo $class_sfx; ?>"<?php echo $id; ?>>
			<?php foreach ($list as $i => &$item)
			{
				$class = 'item-' . $item->id;

				if ($item->id == $default_id)
				{
					$class .= ' default';
				}


				if (($item->id == $active_id) || ($item->type == 'alias' && $item->params->get('aliasoptions') == $active_id))
				{
					$class .= ' current';
				}

				if (in_array($item->id, $path))
				{
					$class .= ' active';
				}
				elseif ($item->type == 'alias')
				{
					$aliasToId = $item->params->get('aliasoptions');

					if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
					{
						$class .= ' active';
					}
					elseif (in_array($aliasToId, $path))
					{
						$class .= ' alias-parent-active';
					}
				}

				if ($item->type == 'separator')
				{
					$class .= ' divider';
				}

				if ($item->deeper)
				{
					$class .= ' deeper';
				}

				if ($item->parent)
				{
					$class .= ' parent';
				}

				echo '<li class="' . $class . '">';

				switch ($item->type) :
					case 'separator':
					case 'component':
					case 'heading':
					case 'url':
						require JModuleHelper::getLayoutPath('mod_menu', 'mobileMenu_' . $item->type);
						break;

					default:
						require JModuleHelper::getLayoutPath('mod_menu', 'mobileMenu_url');
						break;
				endswitch;

				// The next item is deeper.
				if ($item->deeper)
				{
					echo '<ul class="nav-child unstyled small">';
				}
				// The next item is shallower.
				elseif ($item->shallower)
				{
					echo '</li>';
					echo str_repeat('</ul></li>', $item->level_diff);
				}
				// The next item is on the same level.
				else
				{
					echo '</li>';
				}
			}
			?>

				<li class="<?php print ($_SERVER['REQUEST_URI'] == "/homepage-erstellen-referenzen.html") ? "current active" : "not-active"; ?>"><a href="/homepage-erstellen-referenzen.html">Referenzen</a></li>
				<li class="<?php print ($_SERVER['REQUEST_URI'] == "/homepage-erstellen-preise.html") ? "current active" : "not-active"; ?>"><a href="/homepage-erstellen-preise.html">Preise</a></li>
				<li class="<?php print ($_SERVER['REQUEST_URI'] == "/blog.html") ? "current active" : "not-active"; ?>"><a href="/blog.html">Blog</a></li>
				<li class="<?php print ($_SERVER['REQUEST_URI'] == "/kontakt.html") ? "current active" : "not-active"; ?>"><a href="/kontakt.html">Kontakt</a></li>
				<li class="<?php print ($_SERVER['REQUEST_URI'] == "/impressum.html") ? "current active" : "not-active"; ?>"><a href="/impressum.html">Impressum</a></li>
				<li class="<?php print ($_SERVER['REQUEST_URI'] == "/datenschutz.html") ? "current active" : "not-active"; ?>"><a href="/datenschutz.html">Datenschutz</a></li>
			</ul>
			<form action="/texte-business-webseite.html" method="post" class="form-inline">
				<input type="image" alt="Suchen" class="button" src="/templates/089-template/images/searchButton.gif" onclick="this.form.searchword.focus();"><label for="mod-search-searchword113" class="element-invisible">Suchen ...</label> <input name="searchword" id="mod-search-searchword113" maxlength="200" class="inputbox search-query input-medium" type="search" placeholder="Was suchst Du?">		<input type="hidden" name="task" value="search">
				<input type="hidden" name="option" value="com_search">
				<input type="hidden" name="Itemid" value="221">
			</form>	
		</div>
</div>
<span id="mobMenOpen" onclick="openNav()">
	<h4 class="menuTitle">Men&uuml;</h4>
	<button type="button" class="btn btn-navbar">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
</span>
<script type="text/javascript">
	function openNav() {
	    document.getElementById("myNav").style.width = "100%";
	}

	function closeNav() {
	    document.getElementById("myNav").style.width = "0%";
	}	
</script>