<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<div id="<?php print($detectAgent == "tablet " ? "mobDown" : " "); ?>" class="bottom fullwidth">
<?php if($detectAgent == "tablet " || $detectAgent == "desktop ") : ?>
	<div class="gBannerWrapper">
		<div class="greyBanner">
			<a href="/">
				<img class="logoBottom" src="/images/089logo.png" al="089Webdesign Logo Bild">
			</a>
			<h3 class="bottomSlogan primColor">Wir powern Dich und dein Unternehmen</h3>
		</div>
	</div>
<?php endif; ?>	
	<?php if ($this->countModules('bottomMenu') && ( $detectAgent == "desktop ")) : ?>
		<div class="bottomMenu">
			<jdoc:include type="modules" name="bottomMenu" style="custom" />
		</div>
		<div class="footerMenu">
			<jdoc:include type="modules" name="footerMenu" style="custom" />
		</div>		
	<?php endif ?>
	<div class="bottom-wrap innerwidth">
		<?php if ($this->countModules('bottom1')) : ?>
			<div class="belowContent">
				<jdoc:include type="modules" name="bottom1" style="custom" />
			</div>
		<?php endif ?>	
	</div>
</div>  	

