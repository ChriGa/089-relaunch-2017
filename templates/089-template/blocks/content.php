<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$span = 9;
if ($this->countModules('left') && $this->countModules('right')) $span = 6;
if (!$this->countModules('left') && !$this->countModules('right')) $span = 12;

?>
<div id="<?php print($detectAgent == "phone " ? "mobDown" : " "); ?>" class="fullwidth bckgr">
	<?php if(($detectAgent == "desktop " || $detectAgent == "tablet ") && $this->countModules('mainNav')) : ?>
		<div class="mainNav">
			<jdoc:include type="modules" name="mainNav" style="custom" />
		</div>
	<?php endif; ?>
	<?php if($this->countModules('search') && ($detectAgent == "desktop " || $detectAgent == "tablet ")) : ?>
		<div class="searchWrapper">
			<jdoc:include type="modules" name="search" style="custom" />
		</div>
	<?php endif;?>
	<div class="innerwidth content">
		<jdoc:include type="modules" name="banner" style="xhtml" />
		<div class="row-fluid">
		
			<?php if ($this->countModules('left')) : ?>
				<div class="span3 left">
					<jdoc:include type="modules" name="left" style="xhtml" />
				</div>				
			<?php endif; ?>
			
			<main id="content" role="main" class="<?php echo ($detectAgent == "desktop ") ? "span".$span : ""; ?>">
			<!-- Begin Content -->
			<jdoc:include type="modules" name="position-3" style="custom" />
			<jdoc:include type="message" />
			<jdoc:include type="component" />
			<jdoc:include type="modules" name="position-2" style="none" />
			<!-- End Content -->
			</main>

			
			<?php if ($this->countModules('right')) : ?>
				<div class="span3 right">		
					<jdoc:include type="modules" name="right" style="xhtml" />
				</div>
			<?php endif; ?>
			
		</div>
	</div>
</div>
<?php if($detectAgent == "tablet " && $pageclass != " blogView") : ?>
	<a id="downArrow" href="#mobDown"><img src="/images/arrow-down-mobile.png" alt="089Webdesign Pfeil unten - Bild"></a>
	<a id="upArrow" href="#body"><img src="/images/arrow-up-mobile.png" alt="089Webdesign Pfeil oben - Bild"></a>
	<script type="text/javascript">
		jQuery(function(){
			jQuery('#downArrow').on('click', function(event){ 
				jQuery('body').removeClass('downArrow').addClass('upArrow');
			});
			jQuery('#upArrow').on('click', function(event){
				jQuery('body').removeClass('upArrow').addClass('downArrow');
			});
			  jQuery("#upArrow").on('click', scrollToTop);

		function scrollToTop() { // CG: um auf tablet per upArrow button wieder nach oben zu kommen braucht es hier eigene Funktion
				verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
				element = jQuery('body');
				offset = element.offset();
				offsetTop = offset.top;
				jQuery('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
			}

			var lastY; //cg: je nach touch up down event pfeile ein-und ausblenden :
			jQuery(document).bind('touchmove', function (e){
			     var currentY = e.originalEvent.touches[0].clientY;		 
			     if(currentY  > lastY){
			         jQuery('body').removeClass('upArrow').addClass('downArrow');
			     }else if(currentY < lastY){
			         jQuery('body').removeClass('downArrow').addClass('upArrow');
			     }
			     lastY = currentY;
			     	//console.log(currentY + " / " + lastY);
			});
		});
	</script>
<?php endif; ?>

<?php if ($clientMobile) : ?>
	<script type="text/javascript">
		jQuery(function(){
		  jQuery("#downArrow").on('click', function(event) {
		    if (this.hash !== "") {
		      event.preventDefault();
		      var hash = this.hash;
		      jQuery('html, body').animate({
		        scrollTop: jQuery(hash).offset().top
		      }, 800, function(){
		        window.location.hash = hash;
		      });
		    }
		  });
		});
	</script>
<?php endif; ?>				
<div class="clr"></div>
<?php if($detectAgent == "desktop ") : ?>
	<div id="leftNav">
		<nav class="navLeft">
			<jdoc:include type="modules" name="leftNav" style="custom" />
		</nav>
	</div>
<?php endif; ?>
	<?php if($detectAgent == "desktop " || $detectAgent == "tablet " && $this->countModules('topDesigner')) : ?>
		<div id="topDesigner">
				<jdoc:include type="modules" name="topDesigner" style="custom" />
		</div>
	<?php endif;?>