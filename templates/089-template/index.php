<?php
/**
 * @author   	cg@089webdesign.de
 */
defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
?>
<!DOCTYPE html>
<html lang="de-de"<?php //amp ?>>
<head>
	<?php //CG: weitere Fonts via prefetch oder preload HIER einfügen ?>
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/ttf" href="/templates/089-template/fonts/assistant-regular-webfont.ttf">
	<?php // including head
		include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
</head>

<body id="body" class="site <?php print $detectAgent . ($detect->isMobile() ? "mobile " : " ") . $body_class . ($layout ? $layout." " : '') . $option. ' view-' . $view. ($itemid ? ' itemid-' . $itemid : '') . $pageclass; ?>">
	<!-- Body -->
	<div id="pushfx" class="intro-effect-push">
		<div id="wrapper">
			<div class="fullwidth site_wrapper">
			<div class="aboveBottom">
				<?php			
				// including header
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');														
			if($detectAgent == "phone ") {
				// including menu
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');
				// including mobileHeader "Slider"
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/slider.php');
				?>
			</div>
			<?php
				// including content
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');			
			 } else {
				// including content
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	
				?>
			</div>
			<?php } ?>
			<?php	
				// including bottom
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
				
				// including footer
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
				
				?>					
				
			</div>
		</div>
	</div>
	
	<jdoc:include type="modules" name="debug" style="none" />
<?php 
	/*
	 * CG: script include für main.js wg scroll ani zu finden als Override in /089-template/html/com_content/article/default089.php 
	 * Da die Scripte nur in den Themenbereichen wirksam sein dürfen!
	 */
?>
<?php 
if($detectAgent == "desktop ") : ?>
	<link rel="stylesheet" type="text/css" href="/templates/089-template/css/fx.css">
<?php endif; ?>

	<script type="text/javascript">
		jQuery(document).ready(function() {
			<?php //CG: transform Animation click trigger button: ?>
			<?php if($detectAgent =="desktop ") : ?>
		        
		        var buttonText = jQuery('.trigger').html();
		        
			        jQuery('.trigger').on('click', function(event) {        
			             jQuery('#pushfx').toggleClass('modify');
			             jQuery(this).fadeOut(1000, function() {
			             	jQuery(this).toggleClass('clicked');
			             		jQuery(this).fadeIn(1000);
			             			if(jQuery('.trigger.start').hasClass('clicked') || jQuery('.trigger').hasClass('clicked') ) { jQuery('.trigger').html('Zur&uuml;ck nach oben') }
			             			else { jQuery('.trigger').html(buttonText); }
			             });
						jQuery('.accordion-group:last-child').click('on', function(){
							jQuery('.belowContent').toggleClass('bottomUp');
						});				             
		        });
	        <?php endif; ?>
			<?php if ($clientMobile) : ?> //mobile	
					jQuery.extend(jQuery.lazyLoadXT, { // lazyLoad
						  edgeY:  100,
						  srcAttr: 'data-src'
						});	

			<?php //CG: respsonsive menu - klasse togglen im body um beim offenen Menu scrollen zu vermeiden - wg scrollBottom Script ?>
				jQuery('#mobMenOpen, #mobMenClose').on('click', function(event){
						jQuery('body').toggleClass('mobMenIsOpen');
				});

					<?php else : ?> //desktop
						jQuery.extend(jQuery.lazyLoadXT, {
							  edgeY:  50,
						  	srcAttr: 'data-src'
						});

					<?php // carousel Startseite 
						if ($menu->getActive() == $menu->getDefault()) : ?>						
							jQuery('.carousel').carousel({
							  interval: 3000
							});						
					<?php endif; ?>						    

			<?php endif;  ?>
			
				<?php /* WIR NICHT MEHR BENÖTIGT?? LÖSCHEN!! CG wir brauchen eine eigene body-klasse wenn aus dem Blog eine Detailansicht aufgerufen wird - wg Hintergrund: 
					if(jQuery('.item-page').hasClass('blogView')) {
						jQuery('body').addClass(' blogDetail '); 
					}
					*/ ?>
	
	});
	<?php 
		$youtubePages = array(' startseite', ' webdesign', ' grafikdesign', ' textart', ' fotografie', ' webvideo', ' webaudio', ' promotion');

		foreach ($youtubePages as $ytPage) :	
			if($ytPage == $pageclass) : ?>
			
				function setCookie() {
				    var d = new Date();
				    d.setTime(d.getTime() + (30*24*60*60*1000)); <?php // 30 = der erste value in klammern, steht für Anzahl Tage ?>
				    var expires = "expires="+ d.toUTCString();
				    document.cookie = 'youtubeAccepted=true;' + expires + ";path=/";
				    cookieIsSet = true;
				}
				function getCookie(youtubeAccepted) {
				    var name = youtubeAccepted + "=";
				    var ca = document.cookie.split(';');
				    for(var i = 0; i < ca.length; i++) {
				        var c = ca[i];
				        while (c.charAt(0) == ' ') {
				            c = c.substring(1);
				        }
				        if (c.indexOf(name) == 0) {
				            return c.substring(name.length, c.length);
				        }
				    }
				    return "";
				}				
				function checkCookie() {
					var ytCookieSet = getCookie('youtubeAccepted')
						if(ytCookieSet != "") {
							cookieIsSet = true;
						} else {
							cookieIsSet = false;
						}
					return cookieIsSet;
				}

			<?php endif; ?>	
		<?php endforeach; ?>

	</script>
<?php if(!$clientMobile) : ?>
	<div id="resizeAlarm">
		<p>Das Fenster Ihres Webbrowsers ist zu klein - bitte vergrössern Sie ihr Browser-Fenster um die Inhalte sinnvoll darstellen zu können.</p>
	</div>
<?php endif; ?>	
</body>
</html>
