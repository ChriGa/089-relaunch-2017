<?php
/**
 * @author   	0911webdesgin.de
 * @copyright   Copyright (C) 2015 0911webdesgin.de. All rights reserved.
 * @URL 		https://0911webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
	
<header id="header" class="fullwidth">
	<div class="logoWrapper">
		<a href="/">
			<img src="images/089logo.png" alt="Logo von 089 Webdesign">
		</a>
	</div>
<?php if(!$clientMobile) : ?>
	<div class="KontaktBtn">
		<a class="conBtn" href="kontakt.html">Kontakt</a>
	</div>
<?php endif; ?>	
  	<div class="innerwidth ">
		<div class="row-fluid">
			<div class="span8">
				<h2 class="slogan primColor">Das Powerb&uuml;ro für Web &amp; Media</h2>
			</div>        			
			<div class="span4 vcardHolder">
				<div class="vcard clr">			  
					  <p class="tel"><a class="primColor" href="tel:+4981413153275">Tel: 08141 - 315 32 75</a></p>
					  <p class="adr"><span class="street-address">Schlesierstr. 4</span><br />
					    <span class="postal-code">82256 </span><span class="region">F&uuml;rstenfeldbruck</span>
					  </p>
				</div>
				<h3 class="motto">Sag was du brauchst</h3>
			</div>
		</div>
 	</div> 
<?php /* if($clientMobile && $this->countModules('search')) : ?>
	<div class="flex">
		<div class="KontaktBtn">
			<a class="conBtn" href="kontakt.html">E-MAIL</a>
		</div>
		<div class="searchWrapper">
			<jdoc:include type="modules" name="search" style="custom" />
		</div>
	</div>
<?php endif;  */?>	 	   
</header>
